from tdb.models import UserQueries, RelevantTweets, Messages
import pytz
from datetime import datetime
from typing import List, Optional
import urllib.request
from util.general_util import get_media_path


def process_tweet_object(tweet, model: str) -> UserQueries or RelevantTweets:
    """
    :param tweet: tweet obj received from twitter API
    :param model:
    :return: UserQueries or RelevantTweets
    """
    #print(tweet)
    if model == 'UserQueries':
        tweet_obj = UserQueries()
        tweet_obj.relevant_tweet_id = get_relevant_tweet_id(tweet)
    else:
        tweet_obj = RelevantTweets()

    tweet_obj.tweet_id = tweet.id

    ts = datetime.strptime(
        tweet.created_at,
        '%a %b %d %H:%M:%S +0000 %Y').replace(tzinfo=pytz.UTC)
    tweet_obj.timestamp = ts

    try:
        tweet_obj.text = tweet.full_text
    except:
        pass

    try:
        tweet_obj.language = tweet.lang
    except:
        pass

    try:
        tweet_obj.retweet_count = tweet.retweet_count
    except:
        tweet_obj.retweet_count = 0

    try:
        hashtags = tweet.hashtags
        str_hashtags = ""
        for hashtag in hashtags:
            str_hashtags += hashtag.text + " "
        tweet_obj.hashtags = str_hashtags
    except:
        pass

    try:
        urls = tweet.urls
        str_urls = ""
        for url in urls:
            str_urls += url.expanded_url + " "
        tweet_obj.url_mentions = str_urls
    except:
        pass

    try:
        user_mentions = tweet.user_mentions
        str_users = ""
        for user in user_mentions:
            str_users += user.screen_name + " "
        tweet_obj.user_mentions = str_users
    except:
        pass

    try:
        media_urls = tweet._json['extended_entities']['media']
        #print(media_urls)
        str_media_urls = ""
        for url in media_urls:
            if url['type'] == 'video' or url['type'] == 'animated_gif':
                str_media_urls += url['video_info']['variants'][0]['url'] + ' '
            else:
                str_media_urls += url['media_url_https'] + ' '
        tweet_obj.media_urls = str_media_urls
    except:
        pass

    try:
        media_types = tweet.media
        str_media_types = ""
        for obj in media_types:
            str_media_types += obj.type + " "
        tweet_obj.media_types = str_media_types
    except:
        pass

    try:
        tweet_obj.in_reply_to_status_id = tweet.in_reply_to_status_id
    except:
        pass

    try:
        tweet_obj.quoted_status_id = tweet.quoted_status_id
    except:
        pass

    try:
        tweet_obj.retweeted_status_id = tweet.retweeted_status.id
    except:
        pass

    tweet_obj.user_id = tweet.user.id
    tweet_obj.user_screen_name = tweet.user.screen_name

    tweet_obj.tweet_url = 'https://twitter.com/{0}/status/{1}'.format(
        tweet.user.screen_name, tweet.id_str)

    return tweet_obj


def get_relevant_tweet_id(tweet_obj) -> int:
    """
    :param tweet_obj:
    :return:
    """
    if tweet_obj.retweeted_status:
        tweet_obj = tweet_obj.retweeted_status

    # Getting Fact-Checking Tweets IDs
    # TODO: handling in_reply_to_status_id
    if tweet_obj.media:
        return tweet_obj.id
    elif tweet_obj.quoted_status_id:
        return tweet_obj.quoted_status_id
    # elif tweet_obj.in_reply_to_status_id:
    #     return tweet_obj.in_reply_to_status_id
    else:
        return tweet_obj.id


def process_message_object(tweet: RelevantTweets) -> List[Messages]:
    """
    :param tweet:
    :return:
    """
    message_list = list()

    try:
        media_urls = tweet.media_urls.split(' ')
        media_types = tweet.media_types.split(' ')
        print(media_urls)
        for idx in range(0, len(media_urls)):
            uri = save_media_file(media_urls[idx], tweet.tweet_id, idx)
            if uri:
                msg = Messages()
                msg.id = tweet.tweet_id
                msg.uri = uri
                if media_types[idx] == 'photo' or media_types[idx] == 'gif':
                    msg.type = 'IMAGE'
                elif media_types[idx] == 'video':
                    msg.type = 'VIDEO'
                else:
                    msg.type = 'OTHER'
                msg.is_synced = False
                msg.timestamp = tweet.timestamp
                msg.text = ' '
                msg.contact = tweet.user_screen_name
                message_list.append(msg)
    except:
        pass

    msg = Messages()
    msg.id = tweet.tweet_id
    msg.type = 'TEXT'
    msg.is_synced = False
    msg.timestamp = tweet.timestamp
    msg.contact = tweet.user_screen_name
    msg.text = tweet.text
    msg.uri = ' '
    message_list.append(msg)

    return message_list


def save_media_file(url: str, tweet_id: int, idx: int) -> Optional[str]:
    """
    """
    try:
        file_name = str(tweet_id) + '_' + str(
            idx) + url[url.rfind('.'):url.rfind('.') + 4]
        file_path = get_media_path().joinpath(file_name)
        urllib.request.urlretrieve(url, str(file_path))
        return file_name
    except:
        return None
