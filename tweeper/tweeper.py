from typing import List
from twitter import Api
from util.credentials import *
from util.twitter_handles import boomlive_handles, altnews_handles
from tdb.dbhelper import get_max_tweet_id, is_present_in_relevant_tweet, is_present_in_user_query
from tdb.models import UserQueries, RelevantTweets, Messages
from loguru import logger
from tweeper.process import process_tweet_object, get_relevant_tweet_id, process_message_object

api = Api(consumer_key=API_KEY,
          consumer_secret=API_SECRET_KEY,
          access_token_key=ACCESS_TOKEN,
          access_token_secret=ACCESS_TOKEN_SECRET,
          sleep_on_rate_limit=True,
          tweet_mode='extended')

#search_query = "AltNews"


def get_tweet_by_id(tweet_id):
    """
    :param tweet_id:
    :return: Tweet Object of specified tweet id
    """
    try:
        result = api.GetStatus(status_id=int(tweet_id))
        return result
    except:
        logger.critical('Failed to Get Tweet')
        return None


def post_reply(message, status_id) -> bool:
    """
    :param message:
    :param status_id:
    :return:
    """
    try:
        api.PostUpdate(status=message, in_reply_to_status_id=status_id)
        return True
    except:
        logger.critical('Failed to Post Tweet')
        return False


def get_relevant_tweets(tweet_ids: List[int]) -> List[RelevantTweets]:
    """
    :param tweet_ids:
    :return:
    """
    logger.info('Getting Relevant Tweets')

    relevant_tweet_list = list()
    for tid in tweet_ids:
        if not is_present_in_relevant_tweet(tid):
            try:
                result = get_tweet_by_id(tid)
                relevant_tweet_list.append(
                        process_tweet_object(result, 'RelevantTweets'))
            except:
                pass
    return relevant_tweet_list


def get_user_queries(search_query) -> List[UserQueries] and List[int]:
    """
    :return:
    """
    logger.info('Getting User Query')

    result = api.GetUserTimeline(screen_name=search_query,
                            count=100)
    user_query_list=list()
    relevant_status_ids = set()
    for tweet_obj in result:

        if not is_present_in_user_query(tweet_obj.id):
            user_query_list.append(process_tweet_object(tweet_obj, 'UserQueries'))

            # Getting Fact-Checking Tweets IDs
            relevant_status_ids.add(tweet_obj.id)

    return user_query_list, list(relevant_status_ids)


def get_latest_tweet_id(search_query):
    """
    To get the latest tweet_id available on twitter for our query
    :return: (integer) tweet id
    """
    try:
        result = api.GetUserTimeline(screen_name=search_query,
                               count=100)
        return result[0].id, result[-1].id
    except:
        logger.critical('Failed to Get Latest Tweet')
        return -1, -1


def get_individual_messages(
        relevant_tweets: List[RelevantTweets]) -> List[Messages]:
    """
    :return:
    """
    logger.info('Getting Individual Messages')

    message_list = list()
    for tweet in relevant_tweets:
        msgs = process_message_object(tweet)
        message_list += msgs
    return message_list
