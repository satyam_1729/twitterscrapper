from time import sleep
from tweeper.tweeper import get_user_queries, get_relevant_tweets, get_individual_messages
from loguru import logger
from tdb.dbhelper import insert_user_query, insert_relevant_tweet, insert_message

logger.add("logs/tweepy_{time}.log", encoding="utf8")
logger.debug("Big Bang!!")
query = ['AltNews','IndiaToday','boomlive_in','SMHoaxSlayer','newslaundry','scroll_in','IndianExpress']
while True:
    logger.debug('New Batch')
    for screen_name in query:
        logger.debug(screen_name)
        user_queries, relevant_tweet_ids = get_user_queries(screen_name)
        relevant_tweets = get_relevant_tweets(relevant_tweet_ids)
        messages = get_individual_messages(relevant_tweets)

        logger.info('Inserting Tweets into Database')
        logger.info(len(user_queries))
        logger.info(len(relevant_tweets))
        logger.info(len(messages))
        for tweet in user_queries:
            insert_user_query(tweet)
        for tweet in relevant_tweets:
            insert_relevant_tweet(tweet)
        for msg in messages:
            insert_message(msg)

    # TODO: sending un-synced messages to server

    logger.info('Batch Finish! Time to sleep.')
    sleep(60)
