from typing import List
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.expression import func
from tdb.models import UserQueries, RelevantTweets, Messages, engine

Session = sessionmaker(bind=engine)


def insert_user_query(tweet: UserQueries) -> None:
    """
    """
    session = Session()
    try:
        session.add(tweet)
        session.commit()
    except:
        pass
    session.close()


def insert_relevant_tweet(tweet: RelevantTweets) -> None:
    """
    """
    session = Session()
    try:
        session.add(tweet)
        session.commit()
    except:
        pass
    session.close()


def insert_message(msg: Messages) -> None:
    """
    """
    session = Session()
    try:
        session.add(msg)
        session.commit()
    except:
        pass
    session.close()


def is_present_in_relevant_tweet(tweet_id: int) -> bool:
    """
    Checks if tweet_id is already in RelevantTweets
    :param tweet_id:
    :return: bool
    """
    session = Session()
    tweet = session.query(RelevantTweets).filter(
        RelevantTweets.tweet_id == tweet_id).first()
    session.close()
    if tweet is not None:
        return True
    else:
        return False


def is_present_in_user_query(tweet_id: int) -> bool:
    """
    Checks if tweet_id is already in UserQueries
    :param tweet_id:
    :return: bool
    """
    session = Session()
    tweet = session.query(UserQueries).filter(
        UserQueries.tweet_id == tweet_id).first()
    session.close()
    if tweet is not None:
        return True
    else:
        return False


def get_max_tweet_id() -> int:
    """
    Gets the maximum tweet_id (i.e. the latest tweet) saved in UserQueries
    :return:
    """
    session = Session()
    tweet_id = session.query(func.max(UserQueries.tweet_id)).first()[0]
    session.close()
    if tweet_id is not None:
        return tweet_id
    else:
        return -1


def get_unsynced_messages() -> List[Messages]:
    """
    :return:
    """
    pass


def mark_as_synced() -> None:
    """
    :return:
    """
    pass
