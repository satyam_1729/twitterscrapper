from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, BIGINT, Enum, Boolean, DateTime
from sqlalchemy import create_engine
from util.enum import MessageType

Base = declarative_base()
engine = create_engine('sqlite:///mytwitter.db')


class Messages(Base):
    """
    The individual messages that are to be sent to server
    """
    __tablename__ = 'messages'

    id = Column(BIGINT, primary_key=True)
    type = Column(Enum(MessageType), nullable=False)
    uri = Column(String, primary_key=True)
    text = Column(String, primary_key=True)
    is_synced = Column(Boolean, default=False)
    timestamp = Column(DateTime, nullable=False)
    contact = Column(String, nullable=False)


class UserQueries(Base):
    """
    The user queries (tweets) like 'Please fact check this' are stored here
    """
    __tablename__ = 'user_queries'

    tweet_id = Column(BIGINT, primary_key=True)
    timestamp = Column(DateTime, nullable=False)
    text = Column(String)
    tweet_url = Column(String)
    language = Column(String)
    retweet_count = Column(BIGINT)
    hashtags = Column(String)
    url_mentions = Column(String)
    user_mentions = Column(String)
    media_urls = Column(String)
    media_types = Column(String)
    in_reply_to_status_id = Column(BIGINT)
    quoted_status_id = Column(BIGINT)
    retweeted_status_id = Column(BIGINT)
    user_id = Column(BIGINT)
    user_screen_name = Column(String)
    relevant_tweet_id = Column(BIGINT)


class RelevantTweets(Base):
    """
    The tweet to be fact-checked is extracted from user-queries and stored here
    """
    __tablename__ = 'relevant_tweets'

    tweet_id = Column(BIGINT, primary_key=True)
    timestamp = Column(DateTime, nullable=False)
    text = Column(String)
    tweet_url = Column(String)
    language = Column(String)
    retweet_count = Column(BIGINT)
    hashtags = Column(String)
    url_mentions = Column(String)
    user_mentions = Column(String)
    media_urls = Column(String)
    media_types = Column(String)
    in_reply_to_status_id = Column(BIGINT)
    quoted_status_id = Column(BIGINT)
    retweeted_status_id = Column(BIGINT)
    user_id = Column(BIGINT)
    user_screen_name = Column(String)


Base.metadata.create_all(engine)
