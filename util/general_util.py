from pathlib import Path


def get_project_root() -> Path:
    """Returns project root folder."""
    return Path(__file__).parent.parent


def get_media_path() -> Path:
    """Returns media folder."""
    return get_project_root().joinpath('media')


SERVER_ADDR = ""
