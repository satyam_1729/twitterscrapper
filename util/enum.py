import enum


class MessageType(enum.Enum):
    TEXT = 1
    IMAGE = 2
    VIDEO = 3
    AUDIO = 4
    PDF = 5
    FILE = 6
    ZIP = 7
    OTHER = 8
